<?php

use App\Models\Brand;
use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Brand::insert([
            [
                "name" => "HP"
            ],
            [
                "name" => "Dell"
            ],
            [
                "name" => "Apple"
            ],
            [
                "name" => "Lenovo"
            ],
            [
                "name" => "Samsung"
            ],
            [
                "name" => "Acer"
            ],
            [
                "name" => "Asus"
            ],
            [
                "name" => "LG"
            ],
            [
                "name" => "Razer"
            ],
            [
                "name" => "Huawei"
            ],
            [
                "name" => "Xiaomi"
            ]
        ]);
    }
}
