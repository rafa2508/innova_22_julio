<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::insert([
            [
                "name" => "iOS"
            ],
            [
                "name" => "Android"
            ],
            [
                "name" => "Liberado"
            ],
            [
                "name" => "Gaming"
            ],
            [
                "name" => "Portatil"
            ],
            [
                "name" => "Celular"
            ],
            [
                "name" => "Tablet"
            ]
        ]);
    }
}
