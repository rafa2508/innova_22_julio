<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $adminRole = Role::create([
            "name" => 'Admin'
        ]);

        $userRole = Role::create([
            "name" => 'User'
        ]);

        User::create([
            "name" => "Admin",
            "username" => "admin",
            "roles_id" => $adminRole->id,
            "password" => Hash::make('admin')
        ]);

        User::create([
            "name" => "User",
            "username" => "user",
            "role_id" => $userRole->id,
            "password" => Hash::make('user')
        ]);
    }
}
