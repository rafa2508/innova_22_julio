(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[10],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Product/Index.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Product/Index.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Shared_Layout__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../Shared/Layout */ "./resources/js/Shared/Layout.vue");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Layout: _Shared_Layout__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      baseUrl: '/products',
      products: [],
      brands: [],
      categories: [],
      mode: '',
      loading: false,
      dialogVisible: false,
      form: {
        name: '',
        description: '',
        brand_id: '',
        model: '',
        categories: [],
        image: [],
        imageUrl: []
      },
      rules: {
        name: {
          required: true,
          message: 'El nombre de la marca es requerida.'
        },
        description: {
          required: true,
          message: 'La descripcion del producto es requerido.'
        },
        brand_id: {
          required: true,
          message: 'La marca del producto es requerida.'
        },
        model: {
          required: true,
          message: 'El modelo de la marca es requerido.'
        },
        categories: {
          required: true,
          message: 'La categoria de la marca es requerido.'
        }
      }
    };
  },
  computed: {
    modalTitle: function modalTitle() {
      return "".concat(this.mode);
    }
  },
  methods: {
    handleAvatarSuccess: function handleAvatarSuccess(res, file) {
      this.form.image = file.raw;
      this.form.imageUrl = URL.createObjectURL(file.raw);
    },
    beforeAvatarUpload: function beforeAvatarUpload(file) {
      var isJPG = file.type === 'image/jpeg';
      var isLt2M = file.size / 1024 / 1024 < 2;

      if (!isJPG) {
        this.$message.error('Avatar picture must be JPG format!');
      }

      if (!isLt2M) {
        this.$message.error('Avatar picture size can not exceed 2MB!');
      }

      return isJPG && isLt2M;
    },
    clearForm: function clearForm() {
      this.$refs.form.resetFields();
    },
    create: function create() {
      this.mode = 'Nuevo producto';
      this.dialogVisible = true;
    },
    edit: function edit(item) {
      this.mode = 'Editar producto';
      this.form = lodash__WEBPACK_IMPORTED_MODULE_1___default.a.cloneDeep(item);
      this.form.categories = this.form.categories.map(function (_ref) {
        var id = _ref.id;
        return id;
      });
      this.form.imageUrl = this.form.image;
      this.dialogVisible = true;
    },
    submit: function submit() {
      var _this = this;

      this.$refs.form.validate(function () {
        var data = new FormData();
        data.append('name', _this.form.name);
        data.append('description', _this.form.description);
        data.append('brand_id', _this.form.brand_id);
        data.append('categories', _this.form.categories);
        data.append('model', _this.form.model);
        data.append('image', _this.form.image);
        _this.loading = true;

        if (!_this.form.id) {
          _this.$inertia.post(_this.baseUrl, data).then(function () {
            _this.products = _this.$page.products;

            _this.$message({
              type: 'success',
              message: 'Creado correctamente.'
            });

            _this.loading = false;
            _this.dialogVisible = false;
          });
        } else {
          _this.$inertia.put(_this.baseUrl + '/' + _this.form.id, data).then(function () {
            _this.products = _this.$page.products;

            _this.$message({
              type: 'success',
              message: 'Guardado correctamente.'
            });

            _this.loading = false;
            _this.dialogVisible = false;
          });
        }
      });
    },
    remove: function remove($id) {
      var _this2 = this;

      this.$confirm('Esta seguro que quiere eliminar a esta Marca?', 'Advertencia', {
        confirmButtonText: 'OK',
        cancelButtonText: 'Cancelar',
        cancelButtonClass: 'el-button--info',
        type: 'warning'
      }).then(function () {
        _this2.$inertia["delete"](_this2.baseUrl + '/' + $id);
      })["catch"](function () {
        _this2.$message({
          type: 'info',
          message: 'Eliminar cancelado'
        });
      });
    }
  },
  mounted: function mounted() {
    if (this.$page.products) {
      this.products = this.$page.products;
    }

    if (this.$page.brands) {
      this.brands = this.$page.brands;
    }

    if (this.$page.categories) {
      this.categories = this.$page.categories;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Shared/Layout.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Shared/Layout.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_avatar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-avatar */ "./node_modules/vue-avatar/dist/vue-avatar.min.js");
/* harmony import */ var vue_avatar__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_avatar__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'layout',
  components: {
    Avatar: vue_avatar__WEBPACK_IMPORTED_MODULE_0___default.a
  },
  data: function data() {
    return {
      menuItems: [],
      name: '',
      role: ''
    };
  },
  methods: {},
  mounted: function mounted() {
    if (this.$page.prop.auth.user) {
      this.name = this.$page.prop.auth.user.name;
      this.role = this.$page.prop.auth.user.role_id;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Product/Index.vue?vue&type=template&id=7fa283b9&":
/*!***********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Product/Index.vue?vue&type=template&id=7fa283b9& ***!
  \***********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("layout", [
    _c(
      "div",
      [
        _c(
          "el-row",
          [
            _c("el-col", { attrs: { span: 12 } }, [
              _c("h1", [_vm._v("Productos")])
            ]),
            _vm._v(" "),
            _c(
              "el-col",
              { staticStyle: { "text-align": "right" }, attrs: { span: 12 } },
              [
                _c(
                  "el-button",
                  { attrs: { type: "primary" }, on: { click: _vm.create } },
                  [_vm._v("Nuevo Producto")]
                )
              ],
              1
            )
          ],
          1
        ),
        _vm._v(" "),
        _c("el-divider"),
        _vm._v(" "),
        _c(
          "el-table",
          { staticStyle: { width: "100%" }, attrs: { data: _vm.products } },
          [
            _c("el-table-column", { attrs: { prop: "name", label: "Nombre" } }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "description", label: "Descripcion" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { label: "Marca" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _vm._v(
                        "\n                    " +
                          _vm._s(scope.row.brand.name) +
                          "\n                "
                      )
                    ]
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "model", label: "Modelo" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { label: "Categorias" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return _vm._l(scope.row.categories, function(cat) {
                      return _c(
                        "span",
                        [
                          _c(
                            "el-tag",
                            {
                              staticStyle: { "margin-right": "5px" },
                              attrs: { effect: "dark" }
                            },
                            [_vm._v(_vm._s(cat.name))]
                          )
                        ],
                        1
                      )
                    })
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { label: "Operaciones" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _c("el-button", {
                        attrs: {
                          icon: "fas fa-pencil-alt",
                          type: "warning",
                          size: "mini"
                        },
                        on: {
                          click: function($event) {
                            return _vm.edit(scope.row)
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("el-button", {
                        attrs: {
                          icon: "fas fa-trash",
                          size: "mini",
                          type: "danger"
                        },
                        on: {
                          click: function($event) {
                            return _vm.remove(scope.row.id)
                          }
                        }
                      })
                    ]
                  }
                }
              ])
            })
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "el-dialog",
          {
            attrs: { title: _vm.modalTitle, visible: _vm.dialogVisible },
            on: {
              close: _vm.clearForm,
              "update:visible": function($event) {
                _vm.dialogVisible = $event
              }
            }
          },
          [
            _c(
              "el-form",
              { ref: "form", attrs: { model: _vm.form, rules: _vm.rules } },
              [
                _c(
                  "el-form-item",
                  { attrs: { label: "Nombre del Productos", prop: "name" } },
                  [
                    _c("el-input", {
                      attrs: { placeholder: "Nombre del Productos" },
                      model: {
                        value: _vm.form.name,
                        callback: function($$v) {
                          _vm.$set(_vm.form, "name", $$v)
                        },
                        expression: "form.name"
                      }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "el-form-item",
                  { attrs: { label: "Descripcion", prop: "description" } },
                  [
                    _c("el-input", {
                      attrs: { placeholder: "Descripcion" },
                      model: {
                        value: _vm.form.description,
                        callback: function($$v) {
                          _vm.$set(_vm.form, "description", $$v)
                        },
                        expression: "form.description"
                      }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "el-row",
                  { attrs: { gutter: 20 } },
                  [
                    _c(
                      "el-col",
                      { attrs: { span: 12 } },
                      [
                        _c(
                          "el-form-item",
                          { attrs: { label: "Marca", prop: "brand_id" } },
                          [
                            _c(
                              "el-select",
                              {
                                staticStyle: { width: "100%" },
                                attrs: { placeholder: "Marca" },
                                model: {
                                  value: _vm.form.brand_id,
                                  callback: function($$v) {
                                    _vm.$set(_vm.form, "brand_id", $$v)
                                  },
                                  expression: "form.brand_id"
                                }
                              },
                              _vm._l(_vm.brands, function(item) {
                                return _c("el-option", {
                                  key: item.id,
                                  attrs: { label: item.name, value: item.id }
                                })
                              }),
                              1
                            )
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "el-col",
                      { attrs: { span: 12 } },
                      [
                        _c(
                          "el-form-item",
                          {
                            attrs: { label: "Categorias", prop: "categories" }
                          },
                          [
                            _c(
                              "el-select",
                              {
                                staticStyle: { width: "100%" },
                                attrs: {
                                  multiple: "",
                                  placeholder: "Categorias"
                                },
                                model: {
                                  value: _vm.form.brand_id,
                                  callback: function($$v) {
                                    _vm.$set(_vm.form, "brand_id", $$v)
                                  },
                                  expression: "form.brand_id"
                                }
                              },
                              _vm._l(_vm.categories, function(item) {
                                return _c("el-option", {
                                  key: item.id,
                                  attrs: { label: item.name, value: item.id }
                                })
                              }),
                              1
                            )
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "el-form-item",
                  { attrs: { label: "Modelo", prop: "model" } },
                  [
                    _c("el-input", {
                      attrs: { placeholder: "Modelo" },
                      model: {
                        value: _vm.form.model,
                        callback: function($$v) {
                          _vm.$set(_vm.form, "model", $$v)
                        },
                        expression: "form.model"
                      }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticStyle: { "text-align": "center" } },
                  [
                    _c(
                      "el-upload",
                      {
                        staticClass: "avatar-uploader",
                        attrs: {
                          action: "/api/product/upload-image",
                          "show-file-list": false,
                          "on-success": _vm.handleAvatarSuccess,
                          "before-upload": _vm.beforeAvatarUpload
                        }
                      },
                      [
                        _vm.form.imageUrl
                          ? _c("img", {
                              staticClass: "avatar",
                              attrs: { src: _vm.form.imageUrl }
                            })
                          : _c("i", {
                              staticClass: "el-icon-plus avatar-uploader-icon"
                            })
                      ]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticStyle: {
                      "text-align": "center",
                      "padding-top": "10px"
                    }
                  },
                  [
                    _c(
                      "el-button",
                      {
                        attrs: { loading: _vm.loading, type: "primary" },
                        on: {
                          click: function($event) {
                            return _vm.submit()
                          }
                        }
                      },
                      [_vm._v("Guardar")]
                    )
                  ],
                  1
                )
              ],
              1
            )
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Shared/Layout.vue?vue&type=template&id=6bf30086&":
/*!*****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Shared/Layout.vue?vue&type=template&id=6bf30086& ***!
  \*****************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "el-container",
    { staticStyle: { height: "100%" } },
    [
      _c(
        "el-header",
        [
          _c(
            "span",
            { staticClass: "page-title", staticStyle: { "font-size": "30px" } },
            [_vm._v("innova")]
          ),
          _vm._v(" "),
          _c(
            "el-row",
            { staticClass: "fa-pull-right" },
            [
              _c(
                "el-dropdown",
                { attrs: { trigger: "click" } },
                [
                  _c("avatar", {
                    attrs: {
                      username: _vm.name,
                      size: 40,
                      "background-color": "#9615db",
                      color: "#ca8aec"
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "el-dropdown-menu",
                    { attrs: { slot: "dropdown" }, slot: "dropdown" },
                    [
                      _c(
                        "a",
                        { attrs: { href: "/logout" } },
                        [_c("el-dropdown-item", [_vm._v("Salir")])],
                        1
                      )
                    ]
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("el-header", {
        staticStyle: { height: "3px", "background-color": "#9615db" }
      }),
      _vm._v(" "),
      _vm.role === 1
        ? _c(
            "el-menu",
            {
              attrs: {
                mode: "horozontal",
                "background-color": "#545c64",
                "text-color": "#fff",
                "active-text-color": "#ca8aec"
              }
            },
            [
              _c("el-menu-item", { attrs: { index: "1" } }, [
                _c("a", { attrs: { href: "/brands" } }, [_vm._v("Marcas")])
              ]),
              _vm._v(" "),
              _c("el-menu-item", { attrs: { index: "2" } }, [
                _c("a", { attrs: { href: "/products" } }, [_vm._v("Productos")])
              ]),
              _vm._v(" "),
              _c("el-menu-item", { attrs: { index: "3" } }, [
                _c("a", { attrs: { href: "/categories" } }, [
                  _vm._v("Categorias")
                ])
              ]),
              _vm._v(" "),
              _c("el-menu-item", { attrs: { index: "4" } }, [
                _c("a", { attrs: { href: "/users" } }, [_vm._v("Usuario")])
              ])
            ],
            1
          )
        : _vm._e(),
      _vm._v(" "),
      _c("el-container", [_c("el-main", [_vm._t("default")], 2)], 1)
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/Pages/Product/Index.vue":
/*!**********************************************!*\
  !*** ./resources/js/Pages/Product/Index.vue ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Index_vue_vue_type_template_id_7fa283b9___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Index.vue?vue&type=template&id=7fa283b9& */ "./resources/js/Pages/Product/Index.vue?vue&type=template&id=7fa283b9&");
/* harmony import */ var _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Index.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Product/Index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Index_vue_vue_type_template_id_7fa283b9___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Index_vue_vue_type_template_id_7fa283b9___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Product/Index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Product/Index.vue?vue&type=script&lang=js&":
/*!***********************************************************************!*\
  !*** ./resources/js/Pages/Product/Index.vue?vue&type=script&lang=js& ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Product/Index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Pages/Product/Index.vue?vue&type=template&id=7fa283b9&":
/*!*****************************************************************************!*\
  !*** ./resources/js/Pages/Product/Index.vue?vue&type=template&id=7fa283b9& ***!
  \*****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_7fa283b9___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=template&id=7fa283b9& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Product/Index.vue?vue&type=template&id=7fa283b9&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_7fa283b9___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_7fa283b9___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/Shared/Layout.vue":
/*!****************************************!*\
  !*** ./resources/js/Shared/Layout.vue ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Layout_vue_vue_type_template_id_6bf30086___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Layout.vue?vue&type=template&id=6bf30086& */ "./resources/js/Shared/Layout.vue?vue&type=template&id=6bf30086&");
/* harmony import */ var _Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Layout.vue?vue&type=script&lang=js& */ "./resources/js/Shared/Layout.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Layout_vue_vue_type_template_id_6bf30086___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Layout_vue_vue_type_template_id_6bf30086___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Shared/Layout.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/Shared/Layout.vue?vue&type=script&lang=js&":
/*!*****************************************************************!*\
  !*** ./resources/js/Shared/Layout.vue?vue&type=script&lang=js& ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Layout.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Shared/Layout.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Shared/Layout.vue?vue&type=template&id=6bf30086&":
/*!***********************************************************************!*\
  !*** ./resources/js/Shared/Layout.vue?vue&type=template&id=6bf30086& ***!
  \***********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_template_id_6bf30086___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Layout.vue?vue&type=template&id=6bf30086& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Shared/Layout.vue?vue&type=template&id=6bf30086&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_template_id_6bf30086___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_template_id_6bf30086___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);