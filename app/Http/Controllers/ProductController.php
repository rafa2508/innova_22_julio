<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        $products = Product::with('brand', 'categories') -> get();
        $brands = Brand::all();
        $categories = Category::all();

        return Inertia::render('Product/Index' , [
           'products' => $products,
           'brand' => $brands,
           'categories' => $categories
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function uploadImage ()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        $product = new Product();
        $product->name = $request->name;
        $product->description = $request->description;
        $product->brand_id = $request->brand_id;
        $product->model = $request->model;

        $destino = 'img/products';
        $image = $request->hasFile('image');
        if ($image) {
            $imageFile = $request->file('image');
            $filename = $request->name . '_' . $request->model . '.' . $imageFile->getClientOriginalExtension();
            $imageFile->move($destino, $filename);
            $product->image = $destino . '/' . $filename;
        }


        $product->save();

        $product->categories()->sync($request->categories);
        DB::commit();

        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        $product =Product::findOrFail($id);
        $product->name = $request->name;
        $product->description = $request->description;
        $product->brand_id = $request->brand_id;
        $product->model = $request->model;
        $product->save();

        $pic = $request->file('file');
        $destino = 'img/products';
        if ($pic) {
            $this->removeProductImage($product);
            $pic = $request->file('file');
            $filename = $request->name . '_' . $request->model . '.' . $pic->getClientOriginalExtension();
            $pic->move($destino, $filename);
            $product->image = $destino . '/' . $filename;
        }

        $product->categories()->sync($request->categories);
        DB::commit();

        return back();
    }

    private function removeProductImage(Product $product)
    {
        if (!empty($product->image) && file_exists(public_path($product->image))) {
            unlink(public_path($product->image));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        $product =Product::findOrFail($id);
        $product->categories()->detach();
        $product->delete();
        DB::commit();

        return back();
    }
}
